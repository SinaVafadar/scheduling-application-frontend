import 'package:flutter/cupertino.dart';

class PollItem {
  late int pollId;
  late String note;
  late String title;
  late String location;
  late Map pollSettings;
  late int limitOptionCount;
  late List<IconData> icons;
  late DateTime timeCreated;
  late List<dynamic> sessions;


  PollItem({
    required this.pollId,
    required this.title,
    required this.note,
    required this.location,
    required this.limitOptionCount,
    required this.timeCreated,
    required this.pollSettings,
    required this.sessions,
    required this.icons,
});
}