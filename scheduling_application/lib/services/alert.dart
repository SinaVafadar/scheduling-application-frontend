import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

Alert oneButtonAlert(dynamic context, String title, String description, String button) {
  return Alert(
    context: context,
    title: title,
    desc: description,
    buttons: [
      DialogButton(
        color: Colors.redAccent,
        child: Text(
          button,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  );
}

Alert oneButtonAlertSuccess(dynamic context, String title, String description, String button) {
  return Alert(
    context: context,
    title: title,
    desc: description,
    buttons: [
      DialogButton(
        color: Colors.greenAccent,
        child: Text(
          button,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  );
}
