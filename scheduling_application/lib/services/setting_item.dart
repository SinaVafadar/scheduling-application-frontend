class SettingItem {
  late String title;
  late String subtitle;

  SettingItem({required this.title, required this.subtitle});
}
