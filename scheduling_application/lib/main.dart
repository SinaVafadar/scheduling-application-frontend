import 'package:flutter/material.dart';

import 'package:scheduling_application/pages/home.dart';
import 'package:scheduling_application/pages/login_signup.dart';
import 'package:scheduling_application/pages/add_poll.dart';
import 'package:scheduling_application/pages/add_vote.dart';
import 'package:scheduling_application/pages/add_user.dart';
import 'package:scheduling_application/pages/add_session.dart';
import 'package:scheduling_application/pages/show_sessions.dart';
import 'package:scheduling_application/pages/settings_user.dart';
import 'package:scheduling_application/pages/settings_poll.dart';

import 'package:scheduling_application/globals.dart' as globals;

void main() {
  String initialRoute = '/login';
  if (globals.username.isNotEmpty) {
    initialRoute = '/home';
  }

  runApp(MaterialApp(
    initialRoute: initialRoute,
    routes: {
      '/home': (context) => Home(),
      '/login': (context) => LoginSignup(),
      '/addPoll': (context) => AddPoll(),
      '/addPoll/addUser': (context) => AddUser(),
      '/addPoll/SettingsPoll': (context) => SettingsPoll(),
      '/addPoll/showSessions': (context) => ShowSessions(),
      '/addPoll/showSessions/addSession': (context) => AddSession(),
      '/addVote': (context) => AddVote(),
      '/settingsUser': (context) => SettingsUser(),
    },
  ));
}
