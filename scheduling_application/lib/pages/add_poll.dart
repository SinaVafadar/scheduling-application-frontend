import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:scheduling_application/globals.dart' as globals;
import 'package:scheduling_application/services/alert.dart' as alert;

class AddPoll extends StatefulWidget {
  @override
  _AddPollState createState() => _AddPollState();
}

class _AddPollState extends State<AddPoll> {
  List<Map> _sessions = [];
  List<String> _users = [];
  int _limitationNumber = 0;
  List<bool> _settingPollsResult = [false, false];
  TextEditingController _noteController = TextEditingController();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _locationController = TextEditingController();

  @override
  void dispose() {
    _titleController.dispose();
    _locationController.dispose();
    _noteController.dispose();
    super.dispose();
  }

  void _createPoll() async {
    List<Map> formattedSessions = [];

    if (_titleController.text == '') {
      alert
          .oneButtonAlert(
              context, 'Submit Poll', 'Title cannot be empty.', 'Oops!')
          .show();
      return;
    } else if (_sessions.isEmpty) {
      alert
          .oneButtonAlert(
              context, 'Submit Poll', 'Sessions cannot be empty.', 'Oops!')
          .show();
      return;
    }

    _sessions.forEach((element) {
      formattedSessions.add({
        'start': element['start'].replaceAll(' ', 'T') + 'Z',
        'end': element['end'].replaceAll(' ', 'T') + 'Z'
      });
    });

    Map data = {
      'title': _titleController.text,
      'location': _locationController.text,
      'note': _noteController.text,
      'is_hidden': true,
      'is_one_vote_participant': _settingPollsResult[0],
      'is_vote_limited': _settingPollsResult[1],
      'limit_option_count': _limitationNumber,
      'permitted_voters': _users + [globals.username],
      'sessions': formattedSessions
    };

    dynamic response = await http.post(
        Uri.parse('${globals.SERVER_ADDRESS}/polls/list_create_time_poll'),
        body: jsonEncode(data),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });

    if (response.statusCode == 201) {
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      Map body = jsonDecode(response.body);
      if (body['permitted_voters'] != null) {
        alert
            .oneButtonAlert(context, 'Submit Poll',
                'User ${body["permitted_voters"][0]}', 'Oops!')
            .show();
      } else {
        alert
            .oneButtonAlert(
                context, 'Submit Poll', '${body["non_field_errors"]}', 'Oops!')
            .show();
      }
    }
  }

  void _addUser() async {
    dynamic result =
        await Navigator.pushNamed(context, '/addPoll/addUser', arguments: {
      'users': _users,
    });
    _users = result['users'];
  }

  void _setPollSettings() async {
    dynamic result = await Navigator.pushNamed(context, '/addPoll/SettingsPoll',
        arguments: {
          'setting_result': _settingPollsResult,
          'limitation_number': _limitationNumber
        });
    _settingPollsResult = result['setting_result'];
    _limitationNumber = result['limitation_number'];
  }

  void _addSession() async {
    dynamic result =
        await Navigator.pushNamed(context, '/addPoll/showSessions', arguments: {
      'sessions': _sessions,
    });
    _sessions = result['sessions'];
  }

  AppBar _appBar() {
    return AppBar(
      title: Text('New Poll'),
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: () => Navigator.of(context).pop(),
      ),
      actions: [
        Padding(
          padding: EdgeInsets.only(right: 10.0),
          child: IconButton(
            onPressed: () {
              _createPoll();
            },
            icon: Icon(Icons.done),
          ),
        ),
      ],
      bottom: PreferredSize(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
              child: TextField(
                controller: _titleController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(5),
                  enabledBorder: UnderlineInputBorder(),
                  hintText: 'Enter Title Here',
                ),
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          preferredSize: Size.fromHeight(85)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: ListTile(
                title: Text('Select dates'),
                leading: Icon(Icons.date_range),
                onTap: _addSession,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: ListTile(
                title: Text('Add Participant'),
                leading: Icon(Icons.person),
                onTap: _addUser,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 8.0, 8.0, 8.0),
              child: Row(
                children: [
                  Text(
                    'Optional',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[600],
                      fontSize: 14.0,
                      letterSpacing: 2.0,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: TextField(
                controller: _locationController,
                decoration: new InputDecoration(
                  icon: new Icon(Icons.location_on),
                  labelText: 'Location',
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: TextField(
                controller: _noteController,
                decoration: new InputDecoration(
                  icon: new Icon(Icons.notes),
                  labelText: 'Note',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: ListTile(
                title: Text('Settings'),
                leading: Icon(Icons.settings),
                onTap: _setPollSettings,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
