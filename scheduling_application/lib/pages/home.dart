import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:scheduling_application/globals.dart' as globals;
import 'package:scheduling_application/services/alert.dart' as alert;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _polls = [];

  @override
  void initState() {
    _retrieveAllPolls();
    super.initState();
  }

  void _retrieveAllPolls() async {
    dynamic response = await http.get(
        Uri.parse('${globals.SERVER_ADDRESS}/polls/list_create_time_poll'),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });
    _polls = jsonDecode(response.body);

    List<int> ids = [];
    _polls.forEach((element) {
      ids.add(element['id']);
    });

    response = await http.get(
        Uri.parse('${globals.SERVER_ADDRESS}/votes/get_my_votes_brief'),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });
    dynamic otherPolls = jsonDecode(response.body);
    otherPolls.forEach((element) {
      if (!ids.contains(element['id'])) {
        _polls.add(element);
      }
    });
    _polls.sort((first, second) => second['id'].compareTo(first['id']));
    setState(() {});
  }

  dynamic _retrieveSinglePoll(int id) async {
    dynamic response = await http.get(
        Uri.parse('${globals.SERVER_ADDRESS}/polls/time_poll_rud/$id'),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });
    return jsonDecode(response.body);
  }

  void _deletePoll(int index) async {
    Alert(
      context: context,
      title: 'Delete Poll',
      desc: 'Are you sure you want to delete this?',
      buttons: [
        DialogButton(
          color: Colors.redAccent,
          child: Text(
            'Yeah!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            _deletePollAction(_polls[index]['id']);
            Navigator.pop(context);
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            'Nope!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  void _deletePollAction(int pollId) async {
    dynamic response = await http.delete(
        Uri.parse('${globals.SERVER_ADDRESS}/polls/time_poll_rud/$pollId'),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });
    if (response.statusCode == 403) {
      alert
          .oneButtonAlert(
              context, 'Delete Poll', 'This is not your poll!', 'Oops!')
          .show();
      return;
    } else {
      _retrieveAllPolls();
      setState(() {});
    }
  }

  String _parseTime(String inputTime) {
    return DateFormat("yyyy-MM-dd – kk:mm")
        .format(DateTime.parse(inputTime.substring(0, 16)));
  }

  Drawer _drawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue[700],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Scheduling Application',
                  style: TextStyle(
                    letterSpacing: 2.0,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 20.0),
                Text(
                  globals.username,
                  style: TextStyle(
                    letterSpacing: 2.0,
                    fontSize: 14.0,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('Polls'),
            leading: Icon(Icons.poll),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/home');
            },
          ),
          ListTile(
            title: Text('Edit Profile'),
            leading: Icon(Icons.edit),
            onTap: () {
              Navigator.pushNamed(context, '/settingsUser');
            },
          ),
          ListTile(
            title: Text('Log Out'),
            leading: Icon(Icons.logout),
            onTap: () {
              globals.token = '';
              globals.username = '';
              Navigator.pushReplacementNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }

  AppBar _appBar() {
    return AppBar(
      backgroundColor: Colors.blueAccent,
      title: Text('Scheduling Application'),
      centerTitle: true,
      elevation: 0.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      drawer: _drawer(),
      appBar: _appBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addPoll');
        },
        child: const Icon(Icons.add),
      ),
      body: ListView.builder(
        itemCount: _polls.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: ListTile(
                onTap: () async {
                  dynamic pollData =
                      await _retrieveSinglePoll(_polls[index]['id']);
                  Navigator.pushNamed(context, '/addVote', arguments: pollData);
                },
                onLongPress: () {
                  _deletePoll(index);
                },
                title: Text(_polls[index]['title']),
                subtitle: Text(
                    'Creation Date: ${_parseTime(_polls[index]["time_created"])}'),
              ),
            ),
          );
        },
      ),
    );
  }
}
