import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:scheduling_application/globals.dart' as globals;
import 'package:scheduling_application/services/alert.dart' as alert;


class SettingsUser extends StatefulWidget {
  const SettingsUser({Key? key}) : super(key: key);

  @override
  _SettingsUserState createState() => _SettingsUserState();
}

class _SettingsUserState extends State<SettingsUser> {

  late TextEditingController _passwordController = TextEditingController();
  late TextEditingController _newPasswordController = TextEditingController();

  @override
  void dispose() {
    _passwordController.dispose();
    _newPasswordController.dispose();
    super.dispose();
  }

  void _changePassword() async {
    if (_passwordController.text == '') {
      alert
          .oneButtonAlert(context, 'Change Password', 'You have to enter you current password.', 'Oops!')
          .show();
      return ;
    } else if (_newPasswordController.text == '') {
      alert
          .oneButtonAlert(context, 'Change Password', 'You have to enter your new password.', 'Oops!')
          .show();
      return ;
    } else if (_passwordController.text == _newPasswordController.text) {
      alert
          .oneButtonAlert(context, 'Change Password', 'New and current passwords are the same.', 'Oops!')
          .show();
      return ;
    }

    Map data = {
      "current_password": _passwordController.text,
      "new_password": _newPasswordController.text,
    };

    dynamic response = await http.post(
        Uri.parse("${globals.SERVER_ADDRESS}/accounting/change_password"),
        body: jsonEncode(data),
        headers: {
          "Content-type": "application/json",
          "Authorization": globals.token
        }
    );

    if (response.statusCode == 204) {
      alert
          .oneButtonAlertSuccess(context, 'Change Password', 'Password changed successfully.', 'Yay!')
          .show();
      return ;
    } else {
      alert
          .oneButtonAlert(context, 'Change Password', 'Entered password does not match yours!', 'Oops!')
          .show();
      return ;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Settings'),
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: TextField(
                      enabled: false,
                      decoration: new InputDecoration(
                        icon: new Icon(Icons.email),
                        labelText: globals.username,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: TextField(
                      obscureText: true,
                      controller: _passwordController,
                      decoration: new InputDecoration(
                        icon: new Icon(Icons.password),
                        labelText: 'Current Password',
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: TextField(
                      obscureText: true,
                      controller: _newPasswordController,
                      decoration: new InputDecoration(
                        icon: new Icon(Icons.lock),
                        labelText: 'New Password',
                      ),
                    ),
                  ),
                  TextButton(
                    child: Text('Change Password'),
                    onPressed: _changePassword,
                  ),
                ],
              )
          ),
        )
    );
  }
}
