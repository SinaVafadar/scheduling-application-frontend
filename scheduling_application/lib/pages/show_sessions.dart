import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ShowSessions extends StatefulWidget {
  const ShowSessions({Key? key}) : super(key: key);

  @override
  _ShowSessionsState createState() => _ShowSessionsState();
}

class _ShowSessionsState extends State<ShowSessions> {

  List<Map> _sessions = [];

  void _addSession() async {
    dynamic result = await Navigator.pushNamed(
        context, '/addPoll/showSessions/addSession');
    _sessions.add({
      'start': result['start'],
      'end': result['end'],
    });
    setState(() {});
  }

  void _submitSessions() {
    Navigator.pop(context, {
      'sessions': _sessions,
    });
  }

  void _deleteSession(int index) {
    Alert(
      context: context,
      title: 'Delete User',
      desc: 'Are you sure you want to delete this user?',
      buttons: [
        DialogButton(
          color: Colors.redAccent,
          child: Text(
            'Yeah!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            _sessions.removeAt(index);
            Navigator.pop(context);
            setState(() {});
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            'Nope!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {Navigator.pop(context);},
          width: 120,
        )
      ],
    ).show();
  }

  String _parseTime(String inputTime) {
    return DateFormat("yyyy-MM-dd – kk:mm")
        .format(DateTime
        .parse(inputTime
        .substring(0, 16)));
  }

  AppBar _appBar() {
    return AppBar(
      title: Text('Dates'),
        automaticallyImplyLeading: false,
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: Row(
                children: [
                  IconButton(
                    onPressed: _addSession,
                    icon: Icon(
                      Icons.add,
                      size: 26.0,
                    ),
                  ),
                  SizedBox(width: 20.0),
                  IconButton(
                    onPressed: _submitSessions,
                    icon: Icon(
                      Icons.done,
                      size: 26.0,
                    ),
                  ),
                ],
              )
          ),
        ]
    );
  }

  @override
  Widget build(BuildContext context) {

    dynamic arguments = ModalRoute.of(context)!.settings.arguments;
    _sessions = _sessions.isNotEmpty ? _sessions : arguments['sessions'];

    return Scaffold(
      appBar: _appBar(),
      body: ListView.builder(
        itemCount: _sessions.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: ListTile(
                onTap: () {},
                onLongPress: () {_deleteSession(index);},
                title: Text(
                  'Start Time: ${_parseTime(_sessions[index]["start"])}\n'
                  'End   Time: ${_parseTime(_sessions[index]["end"])}'
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
