import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_login/flutter_login.dart';
import 'package:scheduling_application/pages/home.dart';
import 'package:scheduling_application/globals.dart' as globals;

class LoginSignup extends StatefulWidget {
  @override
  _LoginSignupState createState() => _LoginSignupState();
}

class _LoginSignupState extends State<LoginSignup> {
  Duration get loginTime => Duration(milliseconds: 0);

  Future<String> _signupUser(LoginData data) {
    return Future.delayed(loginTime).then((_) async {
      Map requestJson = {
        'username': data.name,
        'password': data.password,
        'email': data.name
      };

      Future<Map> response = _sendRequest(requestJson);

      return await response.then((value) {
        if (value['id'] != null) {
          Navigator.pushReplacementNamed(context, '/login');
          return '';
        } else {
          return 'Invalid Credentials!';
        }
      });
    });
  }

  Future<Map> _sendRequest(Map requestJson) async {
    dynamic response = await http.post(
        Uri.parse('${globals.SERVER_ADDRESS}/accounting/signup'),
        body: jsonEncode(requestJson),
        headers: {
          'Content-type': 'application/json',
        });

    return jsonDecode(response.body);
  }

  Future<String> _authUser(LoginData data) {
    return Future.delayed(loginTime).then((_) async {
      Map requestJson = {
        'username': data.name,
        'password': data.password
      };
      dynamic responseJson = await http.post(
          Uri.parse('${globals.SERVER_ADDRESS}/accounting/login'),
          body: jsonEncode(requestJson),
          headers: {
            'Content-type': 'application/json',
          });

      if (responseJson.statusCode == 200) {
        dynamic body = jsonDecode(responseJson.body);
        globals.token = 'JWT ${body["token"]}';
        globals.username = data.name;
        Navigator.pushReplacementNamed(context, '/home');
        return '';
      } else {
        return 'Invalid Credentials!';
      }
    });
  }

  Future<String> _recoverPassword(String name) {
    return Future.delayed(loginTime).then((_) {
      return '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      title: 'Scheduling App',
      onLogin: _authUser,
      onSignup: _signupUser,
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Home(),
        ));
      },
      onRecoverPassword: _recoverPassword,
      hideForgotPasswordButton: true,
    );
  }
}
