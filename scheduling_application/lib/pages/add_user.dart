import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AddUser extends StatefulWidget {
  const AddUser({Key? key}) : super(key: key);

  @override
  _AddUserState createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  List<String> _users = [];
  TextEditingController _emailController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _addEmail() {
    if (_emailController.text != '' &&
        !_users.contains(_emailController.text)) {
      _users.add(_emailController.text);
      _emailController.text = '';
      setState(() {});
    }
  }

  AppBar _appBar() {
    return AppBar(
      title: Text('Add User'),
      automaticallyImplyLeading: false,
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context, {
                  'users': _users,
                });
              },
              child: Icon(
                Icons.done,
                size: 26.0,
              ),
            )),
      ],
    );
  }

  void _deleteEmail(int index) {
    Alert(
      context: context,
      title: 'Delete User',
      desc: 'Are you sure you want to delete this user?',
      buttons: [
        DialogButton(
          color: Colors.redAccent,
          child: Text(
            'Yeah!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            _users.removeAt(index);
            Navigator.pop(context);
            setState(() {});
          },
          width: 120,
        ),
        DialogButton(
          child: Text(
            'Nope!',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          width: 120,
        )
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    dynamic arguments = ModalRoute.of(context)!.settings.arguments;
    _users = _users.isNotEmpty ? _users : arguments['users'];

    return Scaffold(
      appBar: _appBar(),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(15),
              child: TextField(
                controller: _emailController,
                decoration: new InputDecoration(
                  icon: new Icon(Icons.email),
                  labelText: 'Enter Email',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: TextButton(
                child: Text('Add This Email'),
                onPressed: _addEmail,
              ),
            ),
            Divider(color: Colors.black54),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _users.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 1.0, horizontal: 4.0),
                  child: Card(
                    child: ListTile(
                      onTap: () {},
                      onLongPress: () {
                        _deleteEmail(index);
                      },
                      title: Text(_users[index]),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
