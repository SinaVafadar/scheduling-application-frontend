import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:jalali_calendar/jalali_calendar.dart';

import 'package:scheduling_application/services/alert.dart' as alert;


class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  _AddSessionState createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {

  String _startDateValue = '';
  String _endDateValue = '';

  Future _selectStartDate() async {
    String? picked = await jalaliCalendarPicker(
        context: context,
        convertToGregorian: false,
        showTimePicker: true,
        hore24Format: true);
    if (picked != null) setState(() => _startDateValue = picked);
  }

  Future _selectEndDate() async {
    String? picked = await jalaliCalendarPicker(
        context: context,
        convertToGregorian: false,
        showTimePicker: true,
        hore24Format: true);
    if (picked != null) setState(() => _endDateValue = picked);
  }

  void _submitDate() {
    dynamic before = DateTime.parse(_startDateValue);
    dynamic after = DateTime.parse(_endDateValue);
    if (after.isBefore(before) || before == after) {
      alert
          .oneButtonAlert(context, 'Add Date', 'End time cannot be earlier than or equal to the start time.', 'Oops!')
          .show();
      return ;
    }
    Navigator.pop(context, {
      'start': _startDateValue,
      'end': _endDateValue,
    });
  }

  String _parseTime(String inputTime) {
    return DateFormat("yyyy-MM-dd – kk:mm")
        .format(DateTime
        .parse(inputTime
        .substring(0, 16)));
  }

  AppBar _appBar() {
    return AppBar(
      title: Text('Add date'),
      automaticallyImplyLeading: false,
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: _submitDate,
              child: Icon(
                Icons.done,
                size: 26.0,
              ),
            )
        ),
      ],
    );
  }

  Widget _dateTimeShowerSelector() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
          child: Card(
            child: ListTile(
              title: Text(
                  'Start Time: ${
                      _startDateValue == "" ?
                      "Not Selected Yet" :
                      _parseTime(_startDateValue)
                  }'
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(width: double.infinity),
            child: ElevatedButton(
              onPressed: _selectStartDate,
              child: Text('Set start date time'),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
          child: Card(
            child: ListTile(
              title: Text(
                  'End Time: ${
                      _endDateValue == "" ?
                      "Not Selected Yet" :
                      _parseTime(_startDateValue)
                  }'
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(width: double.infinity),
            child: ElevatedButton(
              onPressed: _selectEndDate,
              child: Text('Set end date time'),
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: new Container(
        child: Column(
          children: [
            Center(
              child: Column(
                children: [
                  _dateTimeShowerSelector(),
                ],
              ),
            )
            // Expanded(child: ShowCalender())
          ],
        ),
      ),
    );
  }
}
