import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:scheduling_application/globals.dart' as globals;
import 'package:scheduling_application/services/poll_item.dart';

class AddVote extends StatefulWidget {
  @override
  _AddVoteState createState() => _AddVoteState();
}

class _AddVoteState extends State<AddVote> {

  bool _isCalled = true;
  List<int> _selectedOptions = [];
  PollItem _poll = PollItem(
      pollId: 0,
      title: '',
      note: '',
      location: '',
      limitOptionCount: 0,
      timeCreated: DateTime.now(),
      pollSettings: {},
      sessions: [],
      icons: []
  );

  void _parseData(Map result) {
    List<IconData> icons = [];
    result['sessions'].forEach((element) {
      if (element['votes'].contains(globals.username)) {
        if (!_selectedOptions.contains(element['id'])) {
          _selectedOptions.add(element['id']);
        }
        icons.add(Icons.circle);
      } else {
        icons.add(Icons.circle_outlined);
      }
    });

    _poll = PollItem(
        pollId: result['id'],
        title: result['title'],
        note: result['note'],
        location: result['location'],
        limitOptionCount: result['limit_option_count'],
        timeCreated: DateTime.parse(result['time_created']),
        pollSettings: {
          'is_hidden': result['is_hidden'],
          'is_one_vote_participant': result['is_one_vote_participant'],
          'is_vote_limited': result['is_vote_limited']
        },
        sessions: result['sessions'],
        icons: icons
    );
  }

  void _submitVote() async {
    dynamic postJson = {
      'poll': _poll.pollId,
      'sessions': _selectedOptions
    };
    http.post(Uri.parse('${globals.SERVER_ADDRESS}/votes/vote_session'),
        body: jsonEncode(postJson),
        headers: {
          'Content-type': 'application/json',
          'Authorization': globals.token
        });
    Navigator.pop(context);
  }

  void _selectVote(int index, bool isDisabled) {
    if (!isDisabled) {
      _poll.icons[index] =
      _poll.icons[index] == Icons.circle ? Icons.circle_outlined : Icons
          .circle;
      if (_selectedOptions.contains(_poll.sessions[index]['id'])) {
        _selectedOptions.remove(_poll.sessions[index]['id']);
      } else {
        _selectedOptions.add(_poll.sessions[index]['id']);
      }
      if (_poll.pollSettings['is_one_vote_participant']) {
        _selectedOptions = [_poll.sessions[index]['id']];
        _poll.icons.asMap().forEach((index, element) {
          _poll.icons[index] = Icons.circle_outlined;
        });
        _poll.icons[index] = Icons.circle;
      }
    }
    setState(() {});
  }

  bool _setBeingEnable(int index) {
    if (_poll.sessions[index]['votes'].length >= _poll.limitOptionCount
        && _poll.pollSettings['is_vote_limited']
        && !_selectedOptions.contains(_poll.sessions[index]['id'])) {
      return true;
    }
    return false;
  }

  String _parseTime(String inputTime) {
    return DateFormat("yyyy-MM-dd – kk:mm")
        .format(DateTime
        .parse(inputTime
        .substring(0, 16)));
  }
  
  AppBar _appBar() {
    return AppBar(
      actions: [
        Padding(
          padding: EdgeInsets.only(right: 10.0),
          child: IconButton(
            onPressed: _submitVote,
            icon: Icon(Icons.done),
          ),
        ),
      ],
      bottom: PreferredSize(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
              child: Column(
                children: [
                  Text(
                    _poll.title,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.5,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Text(
                    _poll.note,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 1.2,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Text(
                    _poll.location,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 1.2,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size.fromHeight(105)),
    );
  }

  @override
  Widget build(BuildContext context) {
    Map argument = ModalRoute.of(context)!.settings.arguments as Map;
    if (_isCalled) {
      _parseData(argument);
      _isCalled = false;
    }
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: _appBar(),
      body: ListView.builder(
        itemCount: _poll.sessions.length,
        itemBuilder: (context, index) {
          bool _isDisabled = _setBeingEnable(index);
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: ListTile(
                onTap: () {_selectVote(index, _isDisabled);},
                title: Text(
                    'Start Time: ${_parseTime(_poll.sessions[index]["start"])}\n'
                    'End   Time: ${_parseTime(_poll.sessions[index]["end"])}'
                ),
                leading: Icon(_poll.icons[index]),
              ),
            ),
          );
        },
      ),
    );
  }
}

