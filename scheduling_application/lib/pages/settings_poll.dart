import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';

import 'package:scheduling_application/services/setting_item.dart';
import 'package:scheduling_application/services/alert.dart' as alert;

class SettingsPoll extends StatefulWidget {
  const SettingsPoll({Key? key}) : super(key: key);

  @override
  _SettingsPollState createState() => _SettingsPollState();
}

class _SettingsPollState extends State<SettingsPoll> {
  List<bool> settingResult = [false, false];
  TextEditingController limitationController = TextEditingController();
  List<IconData> icons = [Icons.circle_outlined, Icons.circle_outlined];
  List<SettingItem> items = [
    SettingItem(
        title: 'One Vote Per Participant',
        subtitle:
            'Participants can only select one option. Once they select an option, they cannot choose another.'),
    SettingItem(
        title: 'Limit Votes Per Option',
        subtitle:
            'First come, first served. Once the spots are filled, the option is no longer available.'),
  ];

  @override
  void dispose() {
    limitationController.dispose();
    super.dispose();
  }

  void _initiateOptions() {
    dynamic data = ModalRoute.of(context)!.settings.arguments;
    settingResult = data['setting_result'];
    limitationController.text = '${data["limitation_number"]}';
    settingResult.asMap().forEach((key, value) {
      icons[key] = value ? Icons.circle : Icons.circle_outlined;
    });
    setState(() {});
  }

  void _submitSettings() {
    if (limitationController.text == '' && settingResult[1]) {
      alert
          .oneButtonAlert(context, 'Change Settings',
              'You have to enter limitation count.', 'Oops!')
          .show();
      return;
    }
    Navigator.pop(context, {
      'setting_result': settingResult,
      'limitation_number': int.parse(limitationController.text)
    });
  }

  void _selectOneVoteParticipant() {
    settingResult[0] = !settingResult[0];
    icons[0] = icons[0] == Icons.circle ? Icons.circle_outlined : Icons.circle;
    setState(() {});
  }

  void _selectLimitedNumberOption() {
    settingResult[1] = !settingResult[1];
    icons[1] = icons[1] == Icons.circle ? Icons.circle_outlined : Icons.circle;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _initiateOptions();

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Poll Settings'),
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              onPressed: _submitSettings,
              icon: Icon(Icons.done),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: ListTile(
                minVerticalPadding: 15.0,
                onTap: _selectOneVoteParticipant,
                title: Text(items[0].title),
                subtitle: Text(items[0].subtitle),
                leading: Icon(icons[0]),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: ListTile(
                minVerticalPadding: 15.0,
                onTap: _selectLimitedNumberOption,
                title: Text(items[1].title),
                subtitle: Text(items[1].subtitle),
                leading: Icon(icons[1]),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: TextField(
                  controller: limitationController,
                  enabled: settingResult[1],
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  decoration: new InputDecoration(
                    icon: new Icon(Icons.person),
                    labelText: 'Number of Limitation',
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
